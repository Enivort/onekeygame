using UnityEngine;

public class GameOver : MonoBehaviour
{
    [SerializeField] private GameObject gameOverPannel;
    public void GameIsOver()
    {
        GetComponent<Score>().EndScore();
        gameOverPannel.SetActive(true);
    }
}
