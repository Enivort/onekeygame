using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    [SerializeField] GameObject[] enemyPref;
    [SerializeField] Transform parent;
    private void Start()
    {
        int ran = Random.Range(0, enemyPref.Length);
        Instantiate(enemyPref[ran], parent.position, parent.rotation, parent);
        Debug.Log("call" + ran, gameObject);
        Destroy(this);
    }
}
