using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText, endScore, highScore, endHighScore;
    float score = 0;
    int highscore = 0;
    public void AddScore(float score)
    {
        this.score += score;
        scoreText.text = "Score : " + (int)this.score;
        highscore = this.score > PlayerPrefs.GetInt("Hiscore") ? (int)this.score : PlayerPrefs.GetInt("Hiscore");
        highScore.text = "High score : " + highscore;
    }

    public void EndScore()
    {
        endScore.text = "Score : " + (int)score;
        PlayerPrefs.SetInt("Hiscore", highscore);
        endHighScore.text = "High score : " + highscore;
    }
}
