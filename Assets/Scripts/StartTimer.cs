using System.Collections;
using UnityEngine;
using TMPro;

public class StartTimer : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI countText;

    private float timer;

    void Start()
    {
        Time.timeScale = 0;
        StartCounter();
    }
    public void StartCounter()
    {
        countText.gameObject.SetActive(true);
        timer = 0;
    }

    private void Update()
    {
        timer += Time.unscaledDeltaTime;
        if ((int)timer == 1)
        {
            countText.text = "" + 3;
        }
        if ((int)timer == 2)
        {
            countText.text = "" + 2;
        }
        if ((int)timer == 3)
        {
            countText.text = "" + 1;
        }
        if ((int)timer == 4)
        {
            countText.text = "Start";
            Time.timeScale = 1;
        }
        if ((int)timer == 5)
        {
            countText.gameObject.SetActive(false);
            countText.text = "";
            this.enabled = false;
        }
    }
}
