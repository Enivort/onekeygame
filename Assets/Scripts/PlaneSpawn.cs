using System.Collections.Generic;
using UnityEngine;

public class PlaneSpawn : MonoBehaviour
{
    [SerializeField] private float spawnDistance;
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private List<GameObject> planePref = new List<GameObject>();
    private Transform player;
    private Score score;

    void Update()
    {
        if (player == null)
        {
            player = GameObject.Find("/Player").transform;
            score = GameObject.Find("/Manager").GetComponent<Score>();
        } else if (transform.position.z - player.position.z < spawnDistance)
        {
            score.AddScore(GetComponent<MovePlane>().GetSpeed());
            GameObject selectedPref = null;
            int ran = Random.Range(0, planePref.Count);
            selectedPref = planePref[ran];
            GameObject newPlane = Instantiate(selectedPref, spawnPoint.position, Quaternion.identity);
            newPlane.GetComponent<MovePlane>().SetPlayerAndSpeed(player, GetComponent<MovePlane>().GetSpeed());
            Destroy(this);
        }
    }
}
