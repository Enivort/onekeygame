using UnityEngine;
using UnityEngine.SceneManagement;
[RequireComponent(typeof(StartTimer))]
public class RestartGame : MonoBehaviour
{
    public void Restart()
    {
        SceneManager.LoadScene(0);
        GetComponent<StartTimer>().enabled = true;
        GetComponent<StartTimer>().StartCounter();
    }
}
