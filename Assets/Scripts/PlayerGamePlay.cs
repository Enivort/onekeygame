using UnityEngine;

public class PlayerGamePlay : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private AudioSource walk;
    [SerializeField] private GameObject bulletPref;
    [SerializeField] private Transform bulletSpawnPoint;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private float jumpForce;

    private bool inJumpZone = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (inJumpZone)
            {
                GetComponent<Rigidbody>().velocity = 3*9.81f * Vector3.up;
            }
            else if (Time.timeScale != 0)
            {
                GameObject bullet = Instantiate(bulletPref, bulletSpawnPoint.position, Quaternion.identity);
                bullet.GetComponent<MoveBullet>().SetPlayer(transform);
            }
        }
        Debug.Log(Physics.OverlapSphere(transform.position - Vector3.up * 1.01f, 0.2f, groundLayer).Length);
        if (Physics.OverlapSphere(transform.position - Vector3.up * 1.01f, 0.2f, groundLayer).Length == 0)
        {
            GetComponent<Rigidbody>().velocity -= 9.81f * Vector3.up *10 * Time.deltaTime;
            anim.SetBool("Jump", true);
            if (walk.isPlaying) walk.Stop();
        }
        else
        {
            anim.SetBool("Jump", false);
            if (!walk.isPlaying && Time.timeScale != 0) walk.Play();
            if (Time.timeScale == 0) walk.Stop();
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            Time.timeScale = 0;
            GameObject.Find("/Manager").GetComponent<GameOver>().GameIsOver();
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("JumpZone"))
        {
            inJumpZone = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("JumpZone"))
        {
            inJumpZone = false;
        }
    }
}
