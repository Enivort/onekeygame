using UnityEngine;

public class MoveBullet : MonoBehaviour
{
    [SerializeField] private float speed, destroyLenght;
    private Transform player;
    private void FixedUpdate()
    {
        transform.Translate(new Vector3(0, 0, speed / 5));
        if (player != null && transform.position.z > player.position.z + destroyLenght)
        {
            Destroy(gameObject);
        }
    }

    public void SetPlayer(Transform player)
    {
        this.player = player;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            Debug.Log("Entered in object");
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
