using UnityEngine;

public class SkyBoxMove : MonoBehaviour
{
    [SerializeField] private float speed = 1;
    private float timer = 0;

    private void FixedUpdate()
    {
        timer += Time.deltaTime;
        RenderSettings.skybox.SetFloat("_Rotation", timer * speed);
    }
}
