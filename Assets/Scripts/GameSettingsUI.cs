using UnityEngine;
using UnityEngine.UI;

public class GameSettingsUI : MonoBehaviour
{
    [SerializeField] private GameObject settingsUI;
    [SerializeField] private Button settingsButton;
    [SerializeField] private GameObject gameOverPannel;

    private bool inSettings = false;

    void Start()
    {
        settingsButton.onClick.AddListener(() => InSettings());
    }

    public void InSettings()
    {
        inSettings = !inSettings;
        settingsUI.SetActive(inSettings);
        Time.timeScale = inSettings || gameOverPannel.activeSelf ? 0 : 1;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
