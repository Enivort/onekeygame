using UnityEngine;

public class MovePlane : MonoBehaviour
{
    [SerializeField] private float speed, maxSpeed, destroyLenght;
    [SerializeField] private Transform player;
    [SerializeField] private Renderer[] renderers;
    private void FixedUpdate()
    {
        transform.position += -speed/5 * Vector3.forward;
        foreach (Renderer renderer in renderers)
        {
            foreach (Material material in renderer.materials)
            {
                material.SetVector("_Offset", -transform.position);
            }
        }
        speed += Time.deltaTime * 2/240; // + 2*speed in 240 seconds
        if (player != null && transform.position.z < player.position.z - destroyLenght)
        {
            Destroy(gameObject);
        }
    }

    public void SetPlayerAndSpeed(Transform player, float speed)
    {
        this.player = player;
        this.speed = speed;
    }
    public float GetSpeed()
    {
        return speed;
    }
}
