using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    [Header("Audio Mixer")]
    [SerializeField] private AudioMixer master;
    [SerializeField] private AudioMixerGroup soundMix;
    [SerializeField] private AudioMixerGroup musicMix;
    [Header("Slider")]
    [SerializeField] private Slider globalSlider;
    [SerializeField] private Slider soundSlider;
    [SerializeField] private Slider musicSlider;

    private float globalVolume = 1f;
    private float soundVolume = 0.5f;
    private float musicVolume = 0.25f;

    void Awake()
    {
        globalSlider.minValue = 0; globalSlider.maxValue = 1;
        soundSlider.minValue = 0; soundSlider.maxValue = 1;
        musicSlider.minValue = 0; musicSlider.maxValue = 1;
        
        globalVolume = PlayerPrefs.GetFloat("globalVolume") > 0 ? PlayerPrefs.GetFloat("globalVolume") : globalVolume;
        soundVolume = PlayerPrefs.GetFloat("soundVolume") > 0 ? PlayerPrefs.GetFloat("soundVolume") : soundVolume;
        musicVolume = PlayerPrefs.GetFloat("musicVolume") > 0 ? PlayerPrefs.GetFloat("musicVolume") : musicVolume;
        
        SetPlayerPref("globalVolume", globalVolume);
        SetPlayerPref("soundVolume", soundVolume);
        SetPlayerPref("musicVolume", musicVolume);

        globalSlider.value = globalVolume;
        soundSlider.value = soundVolume;
        musicSlider.value = musicVolume;

        globalSlider.onValueChanged.AddListener(delegate { globalVolume = globalSlider.value; SetPlayerPref("globalVolume", globalSlider.value); ChangeSoundValue(); });
        soundSlider.onValueChanged.AddListener(delegate { soundVolume = soundSlider.value; SetPlayerPref("soundVolume", soundSlider.value); ChangeSoundValue(); });
        musicSlider.onValueChanged.AddListener(delegate { musicVolume = musicSlider.value; SetPlayerPref("musicVolume", musicSlider.value); ChangeSoundValue(); });
    }

    public void SetPlayerPref(string name, float value)
    {
        PlayerPrefs.SetFloat(name, value);
    }
    public void ChangeSoundValue()
    {
        master.SetFloat("MasterVolume", globalVolume * 80 - 80);
        master.SetFloat("SoundsVolume", globalVolume * soundVolume * 80 - 80);
        master.SetFloat("MusicsVolume", globalVolume * musicVolume * 80 - 80);
    }
}
