using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class SoundComponent : MonoBehaviour
{
    private SoundManager soundManager;
    [SerializeField] private AudioSource audioSource;
    [Header("Audio Mixer")]
    [SerializeField] private AudioMixerGroup soundMix;
    [SerializeField] private AudioMixerGroup musicMix;
    [SerializeField] private float startVolume = 0.5f;
    [SerializeField] private bool isMusic = false;
    [SerializeField] private bool playOnAwake = false;
    [SerializeField] private bool loop = false;

    void Start()
    {
        soundManager = FindObjectOfType<SoundManager>();
        audioSource = GetComponent<AudioSource>();
        audioSource.playOnAwake = playOnAwake;
        audioSource.outputAudioMixerGroup = !isMusic ? soundMix : musicMix;
        audioSource.loop = loop;
        /**if (!playOnAwake)
            audioSource.Stop();*/
        soundManager.ChangeSoundValue();
    }

    public void PlayStopSound()
    {
        if (!audioSource.isPlaying) audioSource.Play();
        else audioSource.Stop();
    }

    public void SetSoundValue(float value)
    {
        if (audioSource != null && !isMusic)
            audioSource.volume = startVolume * value;
    }
    public void SetMusicValue(float value)
    {
        if (audioSource != null && isMusic)
            audioSource.volume = startVolume * value;
    }
}
